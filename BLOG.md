# In-app payment processing in Unity using Braintree

Unity 3D allows you to build great games across mobile and web. On iOS and Android, integrating in-app purchases using native library support is amazing, and Unity makes it easier to integrate these libraries. For webgames however, it is a different story. There is no perceived good way to handle in-app purchases in a Unity webgame. This blog post is meant to dispel that myth and introduce the Braintree v.zero SDK. It is very easy to integrate and makes your Unity webgame ready to process in-app payments in a jiffy.

## Scope
For illustrating how to integrate Braintree with Unity, I have built a simple brick breaker game with a twist. When a player hits a particular type of brick (an US currency), she is charged anywhere randomly between one to ten cents using Braintree.

To make this example easily digestible, we will assume that payment instrument has been captured. We will focus on how to integrate Braintree SDK and execute a transaction.

The entire example can be downloaded [here](https://github.com/saranyan/Unity3D-Braintree-Sample). Code snippets for important bits are presented here.

A demo can be found here -
[![Unity Braintree](http://img.youtube.com/vi/uxJJshq0eqA/0.jpg)](http://www.youtube.com/watch?v=uxJJshq0eqA "In-app payments in Unity webgames")

## Integrating Braintree SDK
You need to first compile the Braintree .NET SDK found [here](https://github.com/braintree/braintree_dotnet). A compiled version is included in the example. In the Unity plugin folder, drop the dll.

![Imgur](http://i.imgur.com/0mOPdSf.png)

## Implementation
For the sake of simplicity, only the code related to processing transactions is shown. The code related to the actual game and interaction between game objects can be referred from the example repository.

### Threaded job
The transaction processing needs to happen as a part of a background job to not slow the gameplay. A threaded job is a good way to implement this.

 ```
 public class Job : ThreadedJob
{
	public decimal charge_amt = 0;
	public Transaction t;
	public int t_code;
	Result<Transaction> r;

	protected override void ThreadFunction()
	{
    //replace this with your sandbox credentials
		BraintreeGateway gateway = new BraintreeGateway
		{
			Environment = Braintree.Environment.SANDBOX,
			MerchantId = "xxx",
			PublicKey = "xxx",
			PrivateKey = "xxx"
		};

		//execute payment
		TransactionRequest request = new TransactionRequest
		{
			Amount = (decimal)charge_amt,
			CreditCard = new TransactionCreditCardRequest
			{
				Number = "4111111111111111",
				ExpirationDate = "05/2012"
			}
		};

		Result<Transaction> result = gateway.Transaction.Sale(request);

		if (result.IsSuccess())
		{
			t = result.Target;
			t_code = 1;

		}
		else if (result.Transaction != null)
		{
			t = result.Transaction;
			t_code = 2;
		}
		else
		{
			t_code = 3;
			r = result;
		}


	}
	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		Debug.Log ("Finished transaction");
		if(t_code == 1){
			Debug.Log("Success!: " + t.Id);
		}
		else if(t_code == 2){
			Debug.Log("Error processing transaction:");
			Debug.Log("  Status: " + t.Status);
			Debug.Log("  Code: " + t.ProcessorResponseCode);
			Debug.Log("  Text: " + t.ProcessorResponseText);
		}
		else if(t_code == 3){
			foreach (ValidationError error in r.Errors.DeepAll())
			{
				Debug.Log("Attribute: " + error.Attribute);
				Debug.Log("  Code: " + error.Code);
				Debug.Log("  Message: " + error.Message);
			}
		}
	}
}

```
Running this job executes a transaction in the background. A sample script might look like below -

```
using UnityEngine;
using System.Collections;

public class SomeCollision : MonoBehaviour {

	public Ball ball;
	Job myJob;
	// Use this for initialization
	void Start () {
		myJob = new Job();
    //for this example we will charge a random Amount
		myJob.charge_amt = (decimal)(Random.Range (0,10)/100.0);
	}

	void Update()
	{
		if (myJob != null)
		{
			if (myJob.Update())
			{
				// Alternative to the OnFinished callback
				myJob = null;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D c){
		if(myJob == null){
			myJob = new Job();
			myJob.charge_amt = (decimal)(Random.Range (0,10)/100.0);
		}
		myJob.Start();
	}
}
```

In the above code, I just trigger a transaction when the ball collides the sprite representing US currency. You can trigger on any game action.

### Using this
As mentioned above, for the sake of simplicity, this example focusses purely on the payment processing part. Obviously, for a real world use case, you want to capture payment somehow and tokenize it and use it to execute transactions. And, that part is easy to do.

So, hack on this and let us know if you have any questions or build something cool.
