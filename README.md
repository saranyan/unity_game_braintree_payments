# About
Sample app demonstrating Braintree v.zero integration with Unity 3D.

# Why
Opens up possibilties of easily integrating in-game payments/charges via Braintree

# Scope
This is a working example that processes payments through a sample credit card. If you have access to Unity Pro, then this can easily be extended to include a HTML component that displays iFrame drop-in for Braintree v.zero to take payment as a User input.

# What
It is a proof of concept brick builder game with a sprite representing US currency in the middle of the bricks. Hitting that brick charges the player between 1 and 10 cents using Braintree.

#Demo video
[![Demo](http://i.imgur.com/nhDOpZs.png)](https://youtu.be/uxJJshq0eqA "Demo")


Have fun!
Questions - reach out @saranyan
