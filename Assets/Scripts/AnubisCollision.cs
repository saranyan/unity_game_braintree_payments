﻿using UnityEngine;
using System.Collections;


public class AnubisCollision : MonoBehaviour {
	
	public Ball ball;
	Job myJob;
	// Use this for initialization
	void Start () {
		myJob = new Job();
		myJob.charge_amt = (decimal)(Random.Range (0,10)/100.0);
	}
	
	void Update()
	{
		if (myJob != null)
		{
			if (myJob.Update())
			{
				// Alternative to the OnFinished callback
				myJob = null;
			}
		}
	}
	
	void OnCollisionEnter2D(Collision2D c){
		if(myJob == null){
			myJob = new Job();
			myJob.charge_amt = (decimal)(Random.Range (0,10)/100.0);
		}
		myJob.Start();
	}	
}
