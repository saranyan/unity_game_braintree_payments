﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void loadLevel(string s){
		print ("Loaded level " + s);
		Application.LoadLevel(s);
	}
	
	public void quitLevel(string s){
		print ("quit level " + s);
		Application.Quit();
	}
	
}
