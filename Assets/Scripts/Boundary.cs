﻿using UnityEngine;
using System.Collections;

public class Boundary : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D c){
		
		if(c.gameObject.name == "Ball"){
			if(this.gameObject.name == "Left Boundary"){
				Debug.Log ("Ball hits left!");
			}
			if(this.gameObject.name == "Right Boundary"){
				Debug.Log ("Ball hits right!");
			}
			
		}
		//Debug.Log ("Collision !");
	}
}
