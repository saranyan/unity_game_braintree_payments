﻿using UnityEngine;
using System.Collections;
using Braintree;

public class Job : ThreadedJob
{	
	public decimal charge_amt = 0;
	public Transaction t; 
	public int t_code;
	Result<Transaction> r;
	
	protected override void ThreadFunction()
	{
		BraintreeGateway gateway = new BraintreeGateway
		{
			Environment = Braintree.Environment.SANDBOX,
			MerchantId = "xxx",
			PublicKey = "xxx",
			PrivateKey = "xxx"
		};
		System.Console.WriteLine("in the thread");
		// Do your threaded task. DON'T use the Unity API here
		//execute payment
		TransactionRequest request = new TransactionRequest
		{
			Amount = (decimal)charge_amt,
			CreditCard = new TransactionCreditCardRequest
			{
				Number = "4111111111111111",
				ExpirationDate = "05/2012"
			}
		};
		
		Result<Transaction> result = gateway.Transaction.Sale(request);
		
		if (result.IsSuccess())
		{
			t = result.Target;
			t_code = 1;
			
		}
		else if (result.Transaction != null)
		{
			t = result.Transaction;
			t_code = 2;
		}
		else
		{
			t_code = 3;
			r = result;
		}
		
		
	}
	protected override void OnFinished()
	{
		// This is executed by the Unity main thread when the job is finished
		Debug.Log ("Finished transaction");
		if(t_code == 1){
			Debug.Log("Success!: " + t.Id);
		}
		else if(t_code == 2){
			Debug.Log("Error processing transaction:");
			Debug.Log("  Status: " + t.Status);
			Debug.Log("  Code: " + t.ProcessorResponseCode);
			Debug.Log("  Text: " + t.ProcessorResponseText);
		}
		else if(t_code == 3){
			foreach (ValidationError error in r.Errors.DeepAll())
			{
				Debug.Log("Attribute: " + error.Attribute);
				Debug.Log("  Code: " + error.Code);
				Debug.Log("  Message: " + error.Message);
			}
		}
	}
}