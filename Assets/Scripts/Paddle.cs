﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	public float speed = 2.5f;
	public Ball ball;
	bool ball_in_play = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	
	void OnCollisionEnter2D(Collision2D c){
		Debug.Log ("Hit paddle!");
		//c.gameObject.rigidbody2D.AddRelativeForce(transform.up * 20.0f);
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
			transform.position = new Vector3(Mathf.Clamp(transform.position.x,0.5f,15.5f),transform.position.y, transform.position.z);
			
			
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
			transform.position = new Vector3(Mathf.Clamp(transform.position.x,0.5f,15.5f),transform.position.y, transform.position.z);
		}
		
		if(!ball_in_play){
			if (Input.GetKey (KeyCode.Space)){
				ball.rigidbody2D.WakeUp();
				print ("ball in play~!" + Time.frameCount);
				ball_in_play = true;
			}
		}
		
		
	}
}
