﻿using UnityEngine;
using System.Collections;

public class LooseCollider : MonoBehaviour {
	public LevelManager levelManager;
	void OnTriggerEnter2D(Collider2D c){
		levelManager.loadLevel("Lose");
	}
	
}
